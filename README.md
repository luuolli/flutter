# Instalação no linux#

Download do SDK no link
https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.5.4-hotfix.2-stable.tar.xz

No diretório Home do seu linux crie uma pasta chamada "development".

# Extraíndo arquivos pra uma pasta do flutter
Digite os comandos no termina linux

```shell
cd ~/development
tar xf ~/Downloads/flutter_linux_v1.5.4-hotfix.2-stable.tar.xz
```

Após copiado os arquivos execute o comando:

```shell
cd flutter/bin
```

# Adicionano PATH

Na pasta home do seu linux cria um arquivo .bash_profile
dentro desse arquivo cole  seguinte linha e salve o arquivo:

```shell
export PATH="$PATH:~/development/flutter/bin"
source $HOME/.bash_profile*
```

Agora só fazer logout/login para fazer a alteração no path.
Para verificar se o flutter está funcionando execute no terminal:

```shell
flutter
```

Com sucesso aparecerá vários comandos relacionados ao flutter no terminal


# Caso o comando flutter não seja encontrado no terminal depois refeito o login

Abra o arquivo .bashrc localizado na pasta home do seu linux e adicione as seguintes linhas ao final do arquivo

```bash
if [ -f ~/.bash_profile ]; then
	. ~/.bash_profile 
fi
```

Salve e faça logout/login novamente e teste o comandno *flutter* no terminal